#include<stdio.h>
#include"board.h"
#include"peripherals.h"
#include"pin_mux.h"
#include"clock_config.h"
#include"LPC55S69_cm33_core0.h"
#include"fsl_debug_console.h"
#include"lcd.h"
#include"arm_math.h"
#include"math.h"
#include"arm_const_structs.h"
#include"fsl_powerquad.h"
#include"fsl_power.h"
#include"back.h"
#include <complex.h>


#define NFFT 256
#define N2FFT (2*NFFT)
#define N3FFT (2*N2FFT)
#define FPR 16000
#define V 340

float x1[N2FFT]={0}, x2[N2FFT]={0};
float G1[N3FFT]={0}, G2[N3FFT]={0},G12[N3FFT]={0},AG12[N2FFT]={0},G[N3FFT]={0},Gtemp=0;
float shift,d,alfa;
float complex theta;

uint16_t dataBuffer1[NFFT]={0},dataBuffer2[NFFT]={0},counter=N2FFT;
uint32_t wr1=0,wr2=0;
bool dataReady1 = false, dataReady2 = false;
lpadc_conv_result_t g_LpadcResultConfigStruct;

typedef struct tab {
	int index;
	float value;

} tab;



/* ADC0_IRQn interrupt handler */
void ADC0_IRQHANDLER(void) {// 0-65535 -> -32768-+32767

	LPADC_GetConvResult(ADC0, &g_LpadcResultConfigStruct, 0U);
	if(!dataReady1){
		dataBuffer1[wr1++]=g_LpadcResultConfigStruct.convValue;
	}
	if(wr1>=NFFT){
		wr1=0;
		dataReady1=true;
	}

	LPADC_GetConvResult(ADC0, &g_LpadcResultConfigStruct, 0U);
	if(!dataReady2){
		dataBuffer2[wr2++]=g_LpadcResultConfigStruct.convValue;
	}
	if(wr2>=NFFT){
		wr2=0;
		dataReady2=true;
	}
}

void gcc_phat(float* x1, float* x2){

	for(int i=0;i<N3FFT;i++){
		G1[i]=0;
		G2[i]=0;
	}
	for(int i=0;i<N2FFT;i++){
		G1[i]=x1[i];
		G2[i]=x2[i];
	}

	arm_cfft_f32(&arm_cfft_sR_f32_len512, G1,0,1); //FFT dla pierwszego mikrofonu
	arm_scale_f32 (G1, 1/256.0, G1, N2FFT);
	arm_cfft_f32(&arm_cfft_sR_f32_len512, G2,0,1); //FFT dla drugiego mikrofonu
	arm_scale_f32 (G2, 1/256.0, G2, N2FFT);

	arm_cmplx_conj_f32(G2,G2,N2FFT);
	arm_cmplx_mult_cmplx_f32(G2,G1,G12,N2FFT);

	arm_cmplx_mag_f32(G12, AG12, N2FFT);


	for (int i=0; i<N2FFT; i++){
		if(AG12[i]<0.000001){
			AG12[i]=0.000001;
		}
	}

	for(int i=0;i<N2FFT;i++){
		G12[2*i]/=AG12[i];
		G12[2*i+1]/=AG12[i];
	}
	arm_cfft_f32(&arm_cfft_sR_f32_len512, G12,1,1);  // odwrotne fft
	arm_scale_f32 (G12, 1/256.0, G12, N3FFT);

	for(int i=0;i<N3FFT;i++){
		G[i]=G12[counter++];

		if(counter==N3FFT){
			counter=0;
		}
	}
	counter=N2FFT;

}

int compare (const void * a, const void * b)
{
	tab *data_1 = (tab *)a;
	tab *data_2 = (tab *)b;

	return ( (data_1->value < data_2->value) -(data_1->value > data_2->value) );
}


void tdoa (float* G){

	tab Tablica[N3FFT];
	for (int i=0; i<N3FFT;i++){
		Tablica[i].value=G[i];
		Tablica[i].index=i;
	}
	qsort(Tablica,N3FFT,sizeof(tab),compare);

	for (int i=0; i<N3FFT;i++){
			G[i]=Tablica[i].value;

		}

	shift=N2FFT-Tablica[0].index;

	d=shift/FPR;
	theta=casin(((d*V)/0.1));
	alfa=creal(theta);
	LCD_Draw_Line(80,127,80+60*sin(alfa),127-60*cos(alfa), 0xff80);
}

int main(void) {
	POWER_DisablePD(kPDRUNCFG_PD_LDOGPADC);
	/* Init board hardware. */
	BOARD_InitBootPins();
	BOARD_InitBootClocks();
	BOARD_InitBootPeripherals();

#ifndef BOARD_INIT_DEBUG_CONSOLE_PERIPHERAL
	/* Init FSL debug console. */
	BOARD_InitDebugConsole();

#endif
	LCD_Init(FLEXCOMM3_PERIPHERAL);

	while(1) {

		LCD_Clear(0x0000);
		LCD_Set_Bitmap((uint16_t*)back_160x128);

		if(dataReady1&&dataReady2) {

			for(int i=0;i<NFFT;i++) {
				x1[2*i] = (dataBuffer1[i]/32768.0)-1; // uint16 to float
				x1[2*i+1] = 0;

				x2[2*i] = (dataBuffer2[i]/32768.0)-1; // uint16 to float
				x2[2*i+1] = 0;
			}

			dataReady1=false;
			dataReady2=false;


			gcc_phat(x1,x2);
			tdoa(G);

			LCD_GramRefresh();
		}
	}
	return 0 ;
}


