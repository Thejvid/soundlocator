################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../startup/boot_multicore_slave.c \
../startup/startup_lpc55s69_cm33_core0.c 

OBJS += \
./startup/boot_multicore_slave.o \
./startup/startup_lpc55s69_cm33_core0.o 

C_DEPS += \
./startup/boot_multicore_slave.d \
./startup/startup_lpc55s69_cm33_core0.d 


# Each subdirectory must supply rules for building sources it contributes
startup/%.o: ../startup/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -DCPU_LPC55S69JBD100 -DCPU_LPC55S69JBD100_cm33 -DCPU_LPC55S69JBD100_cm33_core0 -DFSL_RTOS_BM -DSDK_OS_BAREMETAL -DSDK_DEBUGCONSOLE=1 -DPRINTF_FLOAT_ENABLE=1 -DSERIAL_PORT_TYPE_UART=1 -D__MCUXPRESSO -D__USE_CMSIS -DDEBUG -D__NEWLIB__ -I"/Users/ki_15/Documents/MCUXpressoIDE_11.3.0/workspace/SoundRadar/board" -I"/Users/ki_15/Documents/MCUXpressoIDE_11.3.0/workspace/SoundRadar/source" -I"/Users/ki_15/Documents/MCUXpressoIDE_11.3.0/workspace/SoundRadar/drivers" -I"/Users/ki_15/Documents/MCUXpressoIDE_11.3.0/workspace/SoundRadar/LPC55S69/drivers" -I"/Users/ki_15/Documents/MCUXpressoIDE_11.3.0/workspace/SoundRadar/utilities" -I"/Users/ki_15/Documents/MCUXpressoIDE_11.3.0/workspace/SoundRadar/component/uart" -I"/Users/ki_15/Documents/MCUXpressoIDE_11.3.0/workspace/SoundRadar/component/serial_manager" -I"/Users/ki_15/Documents/MCUXpressoIDE_11.3.0/workspace/SoundRadar/component/lists" -I"/Users/ki_15/Documents/MCUXpressoIDE_11.3.0/workspace/SoundRadar/startup" -I"/Users/ki_15/Documents/MCUXpressoIDE_11.3.0/workspace/SoundRadar/CMSIS" -I"/Users/ki_15/Documents/MCUXpressoIDE_11.3.0/workspace/SoundRadar/device" -O0 -fno-common -g3 -Wall -c -ffunction-sections -fdata-sections -ffreestanding -fno-builtin -fmerge-constants -fmacro-prefix-map="../$(@D)/"=. -mcpu=cortex-m33 -mfpu=fpv5-sp-d16 -mfloat-abi=hard -mthumb -D__NEWLIB__ -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


